﻿using System;
using Decal.Adapter.Wrappers;

namespace WeenieErector.Data.WorldSpawns {
    public class WeenieFrameAngles {
        public double w = 0;
        public double x = 0;
        public double y = 0;
        public double z = 0;

        internal void SetFromOrientation(Vector4Object vector4Object) {
            w = vector4Object.W;
            x = vector4Object.X;
            y = vector4Object.Y;
            z = vector4Object.Z;
        }
    }
}