﻿using System.Collections.Generic;

namespace WeenieErector.Data.WorldSpawns {
    public class LandblockValue {
        public List<Link> links = new List<Link>();
        public List<Weenie> weenies = new List<Weenie>();
    }
}