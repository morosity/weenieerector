﻿using Newtonsoft.Json;
using System;
using WeenieErector.Lib;

namespace WeenieErector.Data.WorldSpawns {
    public class Weenie {
        public string desc = "";
        public uint id = 0;
        public int wcid = 0;
        public WeeniePos pos = new WeeniePos();

        public override string ToString() {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public string GetCoordinateString(uint landblock) {
            int lbxoffset = (int)(Math.Floor((double)(landblock) / (double)(0x1000000))) & 0xFF;
            int lbyoffset = (int)(Math.Floor((double)(landblock) / (double)(0x10000))) & 0xFF;
            var x = Math.Round(((double)(lbxoffset - 0x7F) * 192 + pos.frame.origin.x - 84) / 240d, 5);
            var y = Math.Round(((double)(lbyoffset - 0x7F) * 192 + pos.frame.origin.y - 84) / 240d, 5);

            return Util.FormatCoordinates(x, y);
        }

        internal void TeleportTo() {
            var origin = pos.frame.origin;
            var angle = pos.frame.angles;
            var landblock = Globals.Core.Actions.Landcell.ToString("X8");
            var command = string.Format("/teleloc {0} {1} {2} {3} {4} {5} {6} {7}", landblock, origin.x, origin.y, origin.z, angle.w, angle.x, angle.y, angle.z);

            Util.WriteToChat(command);
            DecalProxy.DispatchChatToBoxWithPluginIntercept(command);
        }

        internal string GetUniqueKey() {
            return string.Format("{0}_{1}", pos.objcell_id, id);
        }
    }
}