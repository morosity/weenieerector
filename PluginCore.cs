﻿using System;
using Decal.Adapter;
using System.IO;
using WeenieErector.Views;
using WeenieErector.Lib;
using WeenieErector.Data;

namespace WeenieErector {
    //Attaches events from core
    [WireUpBaseEvents]

    [FriendlyName("WeenieErector")]
    public class PluginCore : PluginBase {
        protected override void Startup() {
            try {
                Globals.Init("WeenieErector", Host, Core);
                
                Directory.CreateDirectory(Util.GetPluginStoragePath());
                Directory.CreateDirectory(Util.GetWorldSpawnsPath());

                Globals.LocationManager = new LocationManager();
                Globals.SpawnManager = new SpawnManager();
                Globals.MarkerManager = new MarkerManager();
                Globals.View = new MainView();

                Globals.WeenieSource.LoadCache();

                Util.WriteToChat("WeenieErector is UP");

                Globals.Core.RenderFrame += Core_RenderFrame;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                if (Globals.LocationManager != null) Globals.LocationManager.Think();
                if (Globals.SpawnManager != null) Globals.SpawnManager.Think();
                if (Globals.MarkerManager != null) Globals.MarkerManager.Think();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        protected override void Shutdown() {
            try {
                Globals.Core.RenderFrame -= Core_RenderFrame;

                if (Globals.View != null) Globals.View.Dispose();
                if (Globals.MarkerManager != null) Globals.MarkerManager.Dispose();
                if (Globals.SpawnManager != null) Globals.SpawnManager.Dispose();
                if (Globals.LocationManager != null) Globals.LocationManager.Dispose();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }
    }
}