﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeenieErector.Lib {

    public class LandblockChangedEventArgs : EventArgs {
        public uint Landblock { get; set; }
    }

    public class LocationManager : IDisposable {
        private const int UPDATE_INTERVAL_MS = 50;

        private uint currentLandblock = 0;
        private bool isLoggedIn = false;
        private DateTime lastThought = DateTime.UtcNow;

        private bool disposed = false;

        public event EventHandler<LandblockChangedEventArgs> LandblockChanged;

        public LocationManager() {
            Globals.Core.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                isLoggedIn = true;
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void Think() {
            if (isLoggedIn && DateTime.UtcNow - lastThought > TimeSpan.FromMilliseconds(UPDATE_INTERVAL_MS)) {
                lastThought = DateTime.UtcNow;

                if (Util.CurrentLandblock() != 0 && (Util.CurrentLandblock() != currentLandblock)) {
                    currentLandblock = Util.CurrentLandblock();

                    Util.WriteToChat("Landblock changed to: " + currentLandblock);

                    LandblockChangedEventArgs args = new LandblockChangedEventArgs();
                    args.Landblock = currentLandblock;
                    OnLandblockChanged(args);
                }
            }
        }

        protected virtual void OnLandblockChanged(LandblockChangedEventArgs e) {
            LandblockChanged?.Invoke(this, e);
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    Globals.Core.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                }
                disposed = true;
            }
        }
    }
}
