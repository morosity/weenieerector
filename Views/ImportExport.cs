﻿using System;
using System.IO;
using VirindiViewService;
using VirindiViewService.Controls;
using WeenieErector.Lib;

namespace WeenieErector.Views {
    public class ImportExport {
        HudStaticText UISpawnFilesLocation { get; set; }
        HudCombo UIImportSpawnFileList { get; set; }
        HudTextBox UIExportSpawnsFilename { get; set; }
        HudButton UIExportSpawns { get; set; }
        HudButton UIImportSpawns { get; set; }

        public ImportExport(HudView view) {
            UISpawnFilesLocation = (HudStaticText)view["SpawnFilesLocation"];
            UIImportSpawnFileList = (HudCombo)view["ImportSpawnFileList"];
            UIExportSpawnsFilename = (HudTextBox)view["ExportSpawnsFilename"];
            UIExportSpawns = (HudButton)view["ExportSpawns"];
            UIImportSpawns = (HudButton)view["ImportSpawns"];

            UISpawnFilesLocation.Text = Util.GetWorldSpawnsPath();

            RefreshImportFilesList();

            UIExportSpawns.Hit += UIExportSpawns_Hit;
            UIImportSpawns.Hit += UIImportSpawns_Hit;
        }

        private void RefreshImportFilesList() {
            UIImportSpawnFileList.Clear();
            
            DirectoryInfo d = new DirectoryInfo(Util.GetWorldSpawnsPath());
            FileInfo[] files = d.GetFiles("*.json");

            foreach (var file in files) {
                UIImportSpawnFileList.AddItem(file.Name, file.Name);
            }
        }

        private void UIExportSpawns_Hit(object sender, EventArgs e) {
            var filename = UIExportSpawnsFilename.Text;

            if (string.IsNullOrEmpty(filename)) {
                Util.WriteToChat("You must enter a filename to export to.");
                return;
            }

            if (!filename.EndsWith(".json")) {
                filename += ".json";
            }

            Globals.View.EditSpawnsView.SetLoadedFile(filename);

            Globals.SpawnManager.Export(filename);
        }

        private void UIImportSpawns_Hit(object sender, EventArgs e) {
            if (UIImportSpawnFileList.Count > 0) {
                var selectedFile = ((HudStaticText)UIImportSpawnFileList[UIImportSpawnFileList.Current]).Text;

                Globals.SpawnManager.Import(selectedFile);

                Globals.View.EditSpawnsView.SetLoadedFile(selectedFile);
                UIExportSpawnsFilename.Text = selectedFile;
            }
        }
    }
}
