﻿using WeenieErector.Data;
using System;
using System.Text.RegularExpressions;
using VirindiViewService;
using VirindiViewService.Controls;
using WeenieErector.Lib;

namespace WeenieErector.Views {
    public class SpawnWeenie : IDisposable {
        public HudButton UIDownloadWeenies { get; set; }
        public HudList UIWeeniesList { get; set; }
        public HudTextBox UIWeeniesListFilter { get; set; }
        public HudCombo UISpawnWeenieParentLink { get; set; }

        private bool disposed = false;

        public SpawnWeenie(HudView view) {
            UIDownloadWeenies = (HudButton)view["DownloadWeenies"];
            UIWeeniesList = (HudList)view["WeeniesList"];
            UIWeeniesListFilter = (HudTextBox)view["WeeniesListFilter"];
            UISpawnWeenieParentLink = (HudCombo)view["SpawnWeenieParentLink"];

            UIDownloadWeenies.Hit += UIDownloadWeenies_Hit;
            UIWeeniesListFilter.Change += UIWeeniesListFilter_Change;

            UIWeeniesList.Click += UIWeeniesList_Click;

            Globals.WeenieSource.WeeniesLoaded += WeenieSource_WeeniesLoaded;
            Globals.SpawnManager.SpawnsChanged += SpawnManager_SpawnsChanged;
        }

        private void SpawnManager_SpawnsChanged(object sender, SpawnsChangedEventArgs e) {
            try {
                RedrawParentList();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void RedrawParentList() {
            var selected = UISpawnWeenieParentLink.Count > 0 ? ((HudStaticText)UISpawnWeenieParentLink[UISpawnWeenieParentLink.Current]).Text : "0";
            var newSelected = 0;
            var weenies = Globals.SpawnManager.GetWeeniesFromLandblock(Util.CurrentLandblock());
            var index = 1;

            UISpawnWeenieParentLink.Clear();

            UISpawnWeenieParentLink.AddItem("None", "None");

            foreach (var weenie in weenies) {
                var name = string.Format("{0} - {1}", weenie.id, weenie.desc);
                UISpawnWeenieParentLink.AddItem(name, name);
                if (name == selected) {
                    newSelected = index;
                }
                index++;
            }
            UISpawnWeenieParentLink.Current = newSelected;

            Util.WriteToChat("Redraw parents list");
        }

        private void WeenieSource_WeeniesLoaded(object sender, WeeniesLoadedEventArgs e) {
            try {
                RedrawWeenies();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIWeeniesList_Click(object sender, int row, int col) {
            try {
                // spawn "button" column
                if (col != 2) return;

                var rowAccessor = UIWeeniesList[row];
                int wcid = 0;

                if (Int32.TryParse(((HudStaticText)rowAccessor[0]).Text, out wcid)) {
                    int parentId = 0;

                    if (UISpawnWeenieParentLink.Current > 0) {
                        var text = ((HudStaticText)UISpawnWeenieParentLink[UISpawnWeenieParentLink.Current]).Text;
                        Int32.TryParse(text.Split(' ')[0], out parentId);
                    }

                    Util.WriteToChat("spawn: " + Globals.WeenieSource.Weenies[wcid] + " parent: " + parentId);
                    Globals.SpawnManager.Spawn(wcid, parentId);
                }
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIWeeniesListFilter_Change(object sender, EventArgs e) {
            try {
                RedrawWeenies();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        private void UIDownloadWeenies_Hit(object sender, EventArgs e) {
            try {
                Util.WriteToChat("Downloading weenies from content creation google doc... hold on");
                Globals.WeenieSource.Download();
            }
            catch (Exception ex) { Util.LogException(ex); }
        }

        public void RedrawWeenies() {
            UIWeeniesList.ClearRows();

            var filterText = UIWeeniesListFilter.Text;
            Regex searchRegex = new Regex(filterText, RegexOptions.IgnoreCase);

            foreach (var weenie in Globals.WeenieSource.Weenies) {
                if (!string.IsNullOrEmpty(filterText) && !searchRegex.IsMatch(weenie.Value)) {
                    continue;
                }

                var row = UIWeeniesList.AddRow();
                ((HudStaticText)row[0]).Text = weenie.Key.ToString();
                ((HudStaticText)row[1]).Text = weenie.Value;
                ((HudStaticText)row[2]).Text = "[Spawn]";
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                if (disposing) {
                    Globals.WeenieSource.WeeniesLoaded -= WeenieSource_WeeniesLoaded;
                    Globals.SpawnManager.SpawnsChanged -= SpawnManager_SpawnsChanged;
                }
                disposed = true;
            }
        }
    }
}
